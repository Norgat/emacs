
(load "~/.emacs.d/user-modes/mathematica.el")

(setq mathematica-never-start-kernel-with-mode t)


(defvar *mathematica-application-dirictory* 
  "C:/Users/norgat/AppData/Roaming/Mathematica/Applications/")

(defvar *last-math-app* nil)


(defun install-math-app ()
  (interactive)
  (when (file-exists-p *mathematica-application-dirictory*)
    (let* ((path-to-app-source 
	    (read-string "math app: " 
			 (if (eq nil *last-math-app*)
			     (file-name-as-directory
			      (buffer-file-name))
			   *last-math-app*)))
	   (app-name (file-name-nondirectory path-to-app-source))
	   (dest-dir (concat *mathematica-application-dirictory* app-name)))
      (setq *last-math-app* path-to-app-source)
      (copy-directory 
       path-to-app-source
       dest-dir
       nil t t ))))

(provide 'my-mathematica)
