(require 'sr-speedbar)
(defun my-speedbar-settings ()
  "Settings for speedbar."

  (linum-mode 0)

  (setq speedbar-use-images nil)
  (setq speedbar-show-unknown-files t) ; show all files
  (setq speedbar-tag-hierarchy-method '(speedbar-simple-group-tag-hierarchy))
  (setq sr-speedbar-width 30) ; init window size
  (setq sr-speedbar-max-width 70)

  (sr-speedbar-refresh-turn-on)
)

(my-speedbar-settings)

(eval-after-load "speedbar"
  '(my-speedbar-settings))
;(sr-speedbar-open)

(provide 'my-sr-speedbar)
