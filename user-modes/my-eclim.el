
(require 'eclim)
(global-eclim-mode t)
(require 'eclimd)

(custom-set-variables 
 '(eclim-eclipse-dirs
   (list "C:\\eclipse")))

(setq help-at-pt-display-when-idle t)
(setq help-at-pt-timer-delay 0.1)
(help-at-pt-set-timer)

;(require 'ac-emacs-eclim-source)
;(ac-emacs-eclim-config)

(require 'company)
(require 'company-emacs-eclim)
(company-emacs-eclim-setup)
(global-company-mode t)

(setq eclimd-default-workspace "D:\\")
(custom-set-variables 
 '(eclim-executable "C:/eclipse/eclim"))

(custom-set-variables
 '(eclimd-wait-for-process nil))

(provide 'my-eclim)
