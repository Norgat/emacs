
(global-set-key (kbd "M-<up>") 'windmove-up)
(global-set-key (kbd "M-<down>") 'windmove-down)
(global-set-key (kbd "M-<right>") 'windmove-right)
(global-set-key (kbd "M-<left>") 'windmove-left)
(global-set-key (kbd "<f12>") 'buffer-menu)
(global-set-key (kbd "<f1>") 'sr-speedbar-toggle)

(provide 'my-global-keys)
