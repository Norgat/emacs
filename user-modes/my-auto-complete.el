
(require 'auto-complete)
(require 'auto-complete-config)

(set-default 'ac-sources
             '(ac-source-abbrev
               ac-source-dictionary
               ac-source-yasnippet
               ac-source-words-in-buffer
               ac-source-words-in-same-mode-buffers
               ac-source-semantic))

(ac-config-default)
(ac-set-trigger-key "TAB")


(global-auto-complete-mode t)

(provide 'my-auto-complete)
