
(ido-mode t)

(setq tab-width 4)
(setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
(setq indent-tabs-mode t)

(show-paren-mode)
(autopair-global-mode 1) ;i
(global-linum-mode 0)
(desktop-save-mode 1)

(global-hl-line-mode 1)


(setq-default 
 mode-line-format
 (list
  "M: %m   "
  "B: %b   "
  "LC:%l-%c"
  ))


(require 'ahg)

;(require 'yasnippet)
;(yas/global-mode t)


(provide 'my-load-base-modes)
