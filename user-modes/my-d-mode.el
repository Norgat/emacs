
(require 'compile)
(add-to-list
 'compilation-error-regexp-alist
 '("^\\([^ \n]+\\)(\\([0-9]+\\)): \\(?:error\\|.\\|warnin\\(g\\)\\|remar\\(k\\)\\)"
   1 2 nil (3 . 4)))

; modify executable path to include DMD stuff
(setq exec-path (cons "C:/dmd/bin" exec-path))

					; Or this more tricky version that uses a DMDDIR environment variable
					;(setq exec-path (cons 
					;		 (concat (replace-regexp-in-string "\\\\" "/" (getenv "DMDDIR")) "/dmd/bin")
;		 exec-path))
(let ((DMDDIR (getenv "DMDDIR")))
  (setenv "PATH" 
	  (concat DMDDIR "\\dmd2\\windows\\bin;" 
		  DMDDIR "\\dm\\bin;" 
		  DMDDIR "\\dsss\\bin;"
		  (getenv "PATH") ))
  (setenv "LIB" 
	  (concat DMDDIR "\\dmd2\\windows\\lib;" 
		  DMDDIR "\\dm\\lib;" 
		  (getenv "LIB") ))
;  (setenv "INCLUDE" 
;	  (concat DMDDIR "\\dmd\\lib;" 
;		  DMDDIR "\\dm\\lib;" 
;		  (getenv "INCLUDE") ))
  )

(provide 'my-d-mode)
