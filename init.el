;; Section: Initialization of installing packages
(require 'package)
(setq package-archives '(("gnu" . "http://elpa.gnu.org/packages/")
			 ("marmalade" . "http://marmalade-repo.org/packages")
			 ("melpa" . "http://melpa.milkbox.net/packages/")))
(package-initialize)

(defvar my-packages-list 
  (list
   'org 'autopair 'yasnippet 
   'auto-complete 'ahg
   'sr-speedbar 
   ;'emacs-eclim 
   'company 
   'paredit 'paredit-everywhere 'paredit-menu
   ;'cider ; temporary because current mepla version not working
   ))


(defvar refresh-my-packges? nil)

(when refresh-my-packges?
  (dolist (pkg my-packages-list)
    (when (not (package-installed-p pkg))
      (package-install pkg))))


;; Section: Initialization of custom packages and my configs

(defvar local-load-paths 
  '("~/.emacs.d/user-modes/"
    "~/.emacs.d/site/cider/"))

(dolist (path local-load-paths)
  (add-to-list 'load-path path))


(defvar my-config-modes
  (list
   'my-load-base-modes 
   'my-auto-complete
   'my-global-keys 
   'my-sr-speedbar 
   ;'my-eclim 
   'my-mathematica
   'my-d-mode
   'my-clojure))

(dolist (mod my-config-modes) (require mod))



(load "D:/work/quicklisp/slime-helper.el")
(setq inferior-lisp-program "sbcl")
